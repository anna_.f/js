const SIZE_SMALL = {
    id: "small",
    price: 50,
    kal: 20
}

const SIZE_LARGE = {
    id: "large",
    price: 100,
    kal: 40
}

const STUFFING_CHEESE = {
    id: "cheese",
    price: 10,
    kal: 20
}

const STUFFING_SALAD = {
    id: "salad",
    price: 20,
    kal: 5
}

const STUFFING_POTATO = {
    id: "potato",
    price: 15,
    kal: 10
}

const TOPPING_MAYO = {
    id: "mayo",
    price: 20,
    kal: 5
}

const TOPPING_SPICE = {
    id: "spice",
    price: 15,
    kal: 0
}

class Hamburger {
    constructor(size, stuffing) {
        if (size === SIZE_SMALL || size === SIZE_LARGE) {
            this.size = size;
        } else {
            console.error(new Error("Введені не коректні дані. Введіть SIZE_SMALL або SIZE_LARGE "))
        }
        if (stuffing === STUFFING_CHEESE || stuffing === STUFFING_POTATO || stuffing === STUFFING_SALAD) {
            this.stuffing = stuffing;
        } else {
            console.error(new Error("Введені не коректні дані. Введіть STUFFING_CHEESE або STUFFING_POTATO або STUFFING_SALAD"))
        }
        this.toping = [];
    }

    
    addTopping(topping) {
        if (topping === TOPPING_SPICE || topping === TOPPING_MAYO) {
            this.toping.push(topping);
        }
        else {
            console.error(new Error("Введіть TOPPING_SPICE або TOPPING_MAYO"));
        }
        if (this.toping.length > 1) {
            for (let i = 0; i < this.toping.length; i++) {
                if (this.toping[i].id == topping.id) {
                    let index = this.toping.indexOf(this.toping[i]);
                    this.toping.splice(index, 1);
                    console.error(new Error("Топінг вже добавлений"));
                    break;
                } else {
                    break;
                }
            }
        }


    }

    removeTopping(topping) {
        if (topping === TOPPING_SPICE || topping === TOPPING_MAYO) {
            let index = this.toping.indexOf(topping);
            if (index !== -1) {
                this.toping.splice(index, 1);
            } else {
                console.error(new Error("Топінг не добавлений"));
            }
        } else {
            console.error(new Error("Введіть TOPPING_SPICE або TOPPING_MAYO"));

        }
    }

    getToppings() {
        let arrToping = this.toping.map((ele) => { return ele.id });
        return arrToping;
    }

    getSize() {
        return this.size.id;
    }

    getStuffing() {
        return this.stuffing.id;
    }

    calculatePrice() {
        let totalPrice = 0;
        totalPrice += this.size.price + this.stuffing.price;
        this.toping.forEach((e) => {
            totalPrice += e.price;
        })
        return totalPrice;
    }

    calculateCalories() {
        let cal = 0;
        cal += this.size.kal + this.stuffing.kal;
        this.toping.forEach((e) => {
            cal += e.kal;
        })
        return cal;
    }

}

let hamburger = new Hamburger(SIZE_SMALL, STUFFING_CHEESE);

hamburger.addTopping(TOPPING_MAYO);
console.log("Calories: %f", hamburger.calculateCalories());
console.log("Price: %f", hamburger.calculatePrice());
hamburger.addTopping(TOPPING_SPICE);
console.log("Price with sauce: %f", hamburger.calculatePrice());
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE);
hamburger.removeTopping(TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length);
hamburger.addTopping(TOPPING_MAYO);
hamburger.addTopping(TOPPING_MAYO);
console.log(hamburger.getToppings());

