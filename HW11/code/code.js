// Task 1
let div = document.querySelector(".name-secname");
class User {
    constructor(firstName, secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }
    show() {
        return div.innerHTML = `Name: ${this.firstName}<br>LastName: ${this.secondName}`;
    }

}
let user = new User('Ivan', 'Ivanov');
user.show();

// Task 2
let li = document.getElementsByTagName('li')[1];
li.previousElementSibling.style.backgroundColor = "blue";
li.nextElementSibling.style.backgroundColor = "red";

// Task 3
let div2 = document.querySelector(".mouse");
div2.addEventListener("mouseover", (e) => {
    div2.innerHTML = `X: ${e.clientX}/ Y: ${e.clientY}`;
})

// Task 4
let btn = document.querySelector('.button');
let p1 = document.createElement("p");
btn.after(p1);
btn.addEventListener("click", (e) => {
   p1.innerText = `Була натиснута кнопка: ${e.target.value}`;
})

//Task 5
let div3 = document.querySelector('.task5');
div3.addEventListener("mouseover", (e) =>{
e.target.classList.add("left")
})
div3.addEventListener("mouseout",(e)=>{
    e.target.classList.remove("left");
})

// Task 6
let input = document.querySelector(".color");
input.addEventListener("input", (e)=>{
document.body.style.backgroundColor = e.target.value;
})

// Task 7
let login = document.querySelector(".login");
login.addEventListener("input", (e)=>{
    console.dir(e.target.value);
})

// Task 8
let p = document.createElement("p");
let input2 = document.createElement("input");
document.body.append(input2);
input2.after(p)
input2.addEventListener("change",(e)=>{
p.innerText = e.target.value;
})