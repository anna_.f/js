const result = (num1, num2, sign) => {
    switch (sign) {
        case '-':
            res = num1 - num2;
            break;
        case '+':
            res = num1 + num2;
            break;
        case '*':
            res = num1 * num2;
            break;
        case '/':
            if (num2 === 0) {
                res = "error";
            } else {
                res = num1 / num2;
            }
            break;
    }
    return res;
}
const calc = {
    value1: "",
    value2: "",
    operator: "",
    res: "",
    memory: "",
}
const clearCalc = () =>{
    calc.value1 = '';
    calc.value2 = '';
    calc.operator = '';
    calc.res = '';
    
}
const show = (value, el) => {
    el.value = value
}
window.addEventListener("DOMContentLoaded", () => {
    const btn = document.querySelector(".keys"),
        display = document.querySelector("#res");
    let div = document.createElement("div");

    btn.addEventListener("click", function (e) {
        if (e.target.classList.contains("black")) {

            if (calc.value2 === '' && calc.operator === '') {
                if (display.value == calc.res && calc.value2 == '' && calc.operator == '' ) {
                    calc.value1 = '';
                    calc.res = '';
                }
                calc.value1 += e.target.value;
                if (e.target.value === ".") {
                    calc.value1 = "0.";
                }
                show(calc.value1, display);

            }
            if (calc.value1 !== '' && calc.operator !== '') {
                calc.value2 += e.target.value;
                if (e.target.value === ".") {
                    calc.value2 = "0.";
                }
                show(calc.value2, display);
            }
            if (e.target.value === 'C') {
                display.value = '';
                clearCalc();
            }

        }

        if (e.target.classList.contains("pink")) {
            calc.operator = e.target.value;
        }
        if (e.target.value === "=") {
            calc.res = result(Number(calc.value1), Number(calc.value2), calc.operator);
            calc.value1 = calc.res;
            calc.value2 = '';
            calc.operator = '';
            show(calc.res, display);
        }

        if (e.target.value === "m+") {
            calc.memory = display.value;
            div.classList.add("memory")
            div.innerHTML = "m";
            display.after(div);
        }
        if (e.target.value === "mrc") {
            show(calc.memory, display);
            if(display.value !== ''){
                calc.res = '';
                calc.value1 = '';
                calc.value2 = '';
            }
        }
        if (e.target.value === "m-") {
            calc.memory = "";
            div.remove();
        }

    })
})




