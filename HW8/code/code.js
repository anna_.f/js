window.onload = () => {
    
    let creatCircle = document.querySelector("#circle");
    let diameter = document.createElement("input");
    let btn = document.createElement("input");
    btn.setAttribute("type", "button");
    btn.setAttribute("value", "Намалювати");
    diameter.setAttribute("type", "number");
    diameter.setAttribute("id", "diametr")

    creatCircle.onclick = function () {
        creatCircle.replaceWith(diameter);
        diameter.after(btn);

    }
    const creatrDiv = (className) => {
        let circle = document.createElement("div");
        circle.className = "circle";
        circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
        circle.style.borderRadius = "50%";
        circle.style.width = diameter.value + 'px';
        circle.style.height = diameter.value + 'px';
        circle.style.display = "inline-block";
        circle.style.margin = "1px";
        return circle
    }
    btn.onclick = function () {
        for (let i = 0; i < 100; i++) {
            document.body.append(creatrDiv("circle"));;
        }
        diameter.remove();
        btn.remove();
        let [...arrCircle] = document.getElementsByClassName("circle");
        for (let i = 0; i < arrCircle.length; i++) {
            arrCircle[i].onclick = function () {
                arrCircle[i].style.display = "none";
            }
        }
    }

}