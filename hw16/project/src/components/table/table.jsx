import React from "react";

const Table = (props) => {
    return (
        <table>
            <tr>
                <th>Номер</th>
                <th>Назва валюти</th>
                <th>Ціна</th>
            </tr>
        
                {props.data.map((element) => { return (
                    <tr>
                    <td>{element.r030}</td>
                    <td>{element.txt}</td>
                    <td>{element.rate}</td>
                    </tr>
                )})}

        </table>
    )
}

export default Table;