let arr = [];

function Human(name, age) {
    this.name = name;
    this.age = age;
}
arr.push( new Human("Allex", 23));
arr.push( new Human("Leo", 21));
arr.push(new Human("Ivan", 30));
arr.push(new Human("Petro", 19));
console.log(arr.sort(function (a, b) {
    if (a.age > b.age) {
        return 1;
    } if(a.age < b.age){
        return -1;
    }
    return 0;
}))

// task 2
function Human2(name, surname, age){
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.print = function(){
        document.write(`${this.name} ${this.surname} : ${this.age}`)
    }
}
 Human2.enter = function(){
    return new Human2("Ivan", "Ivanov", 25);
 }

 Human2.enter().print();