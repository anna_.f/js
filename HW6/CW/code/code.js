function Calculator(a, b) {
    this.read = function () {
        this.numberOne = parseFloat(prompt("Введіть перше число"));
        this.numberTwo = parseFloat(prompt("Введіть друге число"));
    }

}

Calculator.prototype.sum = function(){
    return console.log(this.numberOne + this.numberTwo) ;
}
Calculator.prototype.sub = function(){
    return console.log(this.numberOne - this.numberTwo);
}
Calculator.prototype.mul = function(){
    return console.log(this.numberOne * this.numberTwo);
}
Calculator.prototype.div = function(){
    if(this.numberTwo === 0){
        return console.error("Ділити на 0 не можна")
    } else {
        return console.log(Math.floor(this.numberOne / this.numberTwo));
    }
   
}
let num = new Calculator();
num.read();
num.sum();
num.mul();
num.sub();
num.div();